# Task

## Getting Started

### Dependencies

* PHP Dev Stack (Preferably Homestead)
* composer
* npm

### Installing

* Extract zip
* Copy content into root folder.
* Change .env file for db settings
* composer install
* composer update
* composer dump-autoload
* php artisan key:generate
* npm install && npm run dev
* php artisan migrate
* php artisan db:seed

### Running

* php artisan dbip:update
* Crontab daily: GetCsv job at 05:00am, UnzipCsv job at 06:00am & InsertCsv job at 07:00am.

## Authors

Hayrettin Gokturk
hayrettin.gokturk@gmail.com