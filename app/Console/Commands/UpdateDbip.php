<?php

namespace App\Console\Commands;

/*
 * This is mandotary somehow.
 */
ini_set('memory_limit', '2048M');

use App\Dbip;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

/**
 * Class UpdateDbip.
 */
class UpdateDbip extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dbip:update';

    /**
     * @var int
     */
    protected $bytes_last = 0;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update dbip in database';
    /**
     * @var \Symfony\Component\Console\Helper\ProgressBar
     */
    private $bar;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $response = Http::get('https://db-ip.com/account/adcda1fff413ac2395a751f7cb7fdd28706cc197/db/ip-to-location-isp/');
        if ($response->failed()) {
            $response->throw();

            return 0;
        }

        $this->info('Downloading file.');
        $ctx = stream_context_create();

        stream_context_set_params($ctx, [
            'notification' => function (
                $notification_code,
                $severity,
                $message,
                $message_code,
                $bytes_transferred,
                $bytes_max
            ) {
                switch ($notification_code) {
                    case STREAM_NOTIFY_RESOLVE:
                    case STREAM_NOTIFY_AUTH_REQUIRED:
                    case STREAM_NOTIFY_COMPLETED:
                        $this->bar->finish();
                        break;
                    case STREAM_NOTIFY_FAILURE:
                    case STREAM_NOTIFY_AUTH_RESULT:
                        dd($notification_code, $severity, $message, $message_code, $bytes_transferred, $bytes_max);
                        break;

                    case STREAM_NOTIFY_REDIRECTED:
                        break;

                    case STREAM_NOTIFY_CONNECT:
                        break;

                    case STREAM_NOTIFY_FILE_SIZE_IS:
                        $this->bar = $this->output->createProgressBar($bytes_max);
                        $this->bar->start();
                        break;

                    case STREAM_NOTIFY_MIME_TYPE_IS:;
                        break;

                    case STREAM_NOTIFY_PROGRESS:
                        if ($bytes_transferred > 0) {
                            $a = $bytes_transferred - $this->bytes_last;
                            $this->bar->advance($a);
                            $this->bytes_last = $bytes_transferred;
                        }
                        break;
                }
            },
        ]);

        $contents = @file_get_contents($response->json()['csv']['url'], false, $ctx);

        if ($contents == false) {
            $this->error('Unable to read file.');

            return 0;
        }

        $file_name = $response->json()['csv']['name'];

        Storage::disk('local')->put($file_name, $contents);

        $this->output->newLine();
        $this->info('Download complete.');
        $this->info('Unzipping file.');

        $file = Storage::disk('local')->path($file_name);

        $buffer_size = 4096; // read 4kb at a time
        $out_file_name = str_replace('.gz', '', $file);

        $file = gzopen($file, 'rb');
        $out_file = fopen($out_file_name, 'wb');

        while (! gzeof($file)) {
            fwrite($out_file, gzread($file, $buffer_size));
        }

        fclose($out_file);
        gzclose($file);

        $this->info('Unzip complete.');

        $count = 0;
        $this->info('Inserting data.');
        $insert_bar = $this->output->createProgressBar($response->json()['csv']['rows']);
        $insert_bar->start();
        $data = [];
        $elem = [];
        $handle = fopen($out_file_name, 'r');
        $regex = <<<'END'
/
  (
    (?: [\x00-\x7F]                 # single-byte sequences   0xxxxxxx
    |   [\xC0-\xDF][\x80-\xBF]      # double-byte sequences   110xxxxx 10xxxxxx
    |   [\xE0-\xEF][\x80-\xBF]{2}   # triple-byte sequences   1110xxxx 10xxxxxx * 2
    |   [\xF0-\xF7][\x80-\xBF]{3}   # quadruple-byte sequence 11110xxx 10xxxxxx * 3 
    ){1,100}                        # ...one or more times
  )
| .                                 # anything else
/x
END;

        if ($handle) {
            while ($line = fgetcsv($handle)) {
                if ($count < 1000) {
                    $elem['ip_start'] = $line[0];
                    $elem['ip_end'] = $line[1];
                    $line2 = substr(trim($line[2]), 0, 2);
                    $elem['continent'] = $line2;
                    $line3 = substr(trim($line[3]), 0, 2);
                    $elem['country'] = $line3;
                    $line4 = substr(trim($line[4]), 0, 80);
                    $elem['stateprov'] = $line4;
                    $line5 = substr(trim($line[5]), 0, 80);
                    $elem['district'] = $line5;
                    $line6 = substr(trim($line[6]), 0, 80);
                    $elem['city'] = $line6;
                    $line7 = substr(trim($line[7]), 0, 20);
                    $elem['zipcode'] = $line7;
                    $elem['latitude'] = $line[8];
                    $elem['longitude'] = $line[9];
                    $elem['geoname_id'] = $line[10];
                    $elem['timezone_offset'] = (float) trim($line[11]);
                    $line12 = substr(trim($line[12]), 0, 64);
                    $elem['timezone_name'] = $line12;
                    $line13 = substr(trim($line[13]), 0, 10);
                    $elem['weather_code'] = $line13;
                    $line14 = substr(trim($line[14]), 0, 128);
                    $line14 = preg_replace($regex, '$1', $line14);
                    $elem['isp_name'] = $line14;
                    $ln15 = trim($line[15]);
                    $elem['as_number'] = $ln15 == '' ? null : (int) $ln15;
                    $ln16 = trim($line[16]);
                    $elem['connection_type'] = $ln16 == '' ? null : $ln16;
                    $line17 = substr(trim($line[17]), 0, 128);
                    $elem['organization_name'] = $line17;
                    $elem['created_at'] = Carbon::now();
                    $elem['updated_at'] = Carbon::now();

                    array_push($data, $elem);
                    $elem = [];
                    $count++;
                } else {
                    if (Dbip::insert($data)) {
                        $insert_bar->advance(1000);
                        $count = 0;
                        $data = [];
                    } else {
                        $this->error('olmadı sanki');
                        break;
                    }
                }
            }
            $insert_bar->finish();
            $this->output->newLine();
            $this->info('Insert complete.');
        }

        return 0;
    }
}
