<?php

namespace App\Console;

use App\Jobs\GetCsv;
use App\Jobs\InsertCsv;
use App\Jobs\UnzipCsv;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

/**
 * Class Kernel.
 */
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();

        /*$schedule->job(new GetCsv)->dailyAt('12:00')->timezone('Europe/Istanbul');
        $schedule->job(new UnzipCsv)->dailyAt('12:10')->timezone('Europe/Istanbul');
        $schedule->job(new InsertCsv)->dailyAt('12:20')->timezone('Europe/Istanbul');*/

        $schedule->job(new GetCsv)->dailyAt('05:00');
        $schedule->job(new UnzipCsv)->dailyAt('06:00');
        $schedule->job(new InsertCsv)->dailyAt('07:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }

    /*protected function scheduleTimezone()
    {
        return 'Europe/Istanbul';
    }*/
}
