<?php

namespace App\Jobs;

use App\Dbip;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class InsertCsv implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $count = 0;

        $data = [];
        $elem = [];

        $file = Storage::disk('local')->path('dbip-full-'.Carbon::now()->format('Y').'-'.Carbon::now()->format('m').'.csv');
        $handle = fopen($file, 'r');
        $regex = <<<'END'
/
  (
    (?: [\x00-\x7F]                 # single-byte sequences   0xxxxxxx
    |   [\xC0-\xDF][\x80-\xBF]      # double-byte sequences   110xxxxx 10xxxxxx
    |   [\xE0-\xEF][\x80-\xBF]{2}   # triple-byte sequences   1110xxxx 10xxxxxx * 2
    |   [\xF0-\xF7][\x80-\xBF]{3}   # quadruple-byte sequence 11110xxx 10xxxxxx * 3 
    ){1,100}                        # ...one or more times
  )
| .                                 # anything else
/x
END;

        if ($handle) {
            while ($line = fgetcsv($handle)) {
                if ($count < 1000) {
                    $elem['ip_start'] = $line[0];
                    $elem['ip_end'] = $line[1];
                    $line2 = substr(trim($line[2]), 0, 2);
                    $elem['continent'] = $line2;
                    $line3 = substr(trim($line[3]), 0, 2);
                    $elem['country'] = $line3;
                    $line4 = substr(trim($line[4]), 0, 80);
                    $elem['stateprov'] = $line4;
                    $line5 = substr(trim($line[5]), 0, 80);
                    $elem['district'] = $line5;
                    $line6 = substr(trim($line[6]), 0, 80);
                    $elem['city'] = $line6;
                    $line7 = substr(trim($line[7]), 0, 20);
                    $elem['zipcode'] = $line7;
                    $elem['latitude'] = $line[8];
                    $elem['longitude'] = $line[9];
                    $elem['geoname_id'] = $line[10];
                    $elem['timezone_offset'] = (float) trim($line[11]);
                    $line12 = substr(trim($line[12]), 0, 64);
                    $elem['timezone_name'] = $line12;
                    $line13 = substr(trim($line[13]), 0, 10);
                    $elem['weather_code'] = $line13;
                    $line14 = substr(trim($line[14]), 0, 128);
                    $line14 = preg_replace($regex, '$1', $line14);
                    $elem['isp_name'] = $line14;
                    $ln15 = trim($line[15]);
                    $elem['as_number'] = $ln15 == '' ? null : (int) $ln15;
                    $ln16 = trim($line[16]);
                    $elem['connection_type'] = $ln16 == '' ? null : $ln16;
                    $line17 = substr(trim($line[17]), 0, 128);
                    $elem['organization_name'] = $line17;
                    $elem['created_at'] = Carbon::now();
                    $elem['updated_at'] = Carbon::now();

                    array_push($data, $elem);
                    $elem = [];
                    $count++;
                } else {
                    if (Dbip::insert($data)) {
                        $count = 0;
                        $data = [];
                    } else {
                        break;
                    }
                }
            }
        }
    }
}
