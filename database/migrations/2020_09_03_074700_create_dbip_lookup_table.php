<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDbipLookupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    /*public function up()
    {
        Schema::create('dbip_lookup', function (Blueprint $table) {
            $table->id();
            $table->ipAddress('ip_start');
            $table->ipAddress('ip_end');
            $table->char('continent', 2);
            $table->char('country', 2);
            $table->char('stateprov', 80);
            $table->char('district', 80);
            $table->char('city', 80);
            $table->char('zipcode', 20);
            $table->float('latitude');
            $table->float('longitude');
            $table->integer('geoname_id')->nullable(true)->unsigned();
            $table->float('timezone_offset');
            $table->char('timezone_name', 64);
            $table->char('weather_code', 10);
            $table->char('isp_name', 128);
            $table->integer('as_number')->nullable(true)->unsigned();
            $table->enum('connection_type', ['dialup','isdn','cable','dsl','fttx','wireless'])->nullable(true);
            $table->char('organization_name', 128);
            $table->timestamps();
        });
    }*/

    public function up()
    {
        Schema::create('dbip_lookup', function (Blueprint $table) {
            $table->id();
            $table->ipAddress('ip_start')->nullable(true);
            $table->ipAddress('ip_end')->nullable(true);
            $table->char('continent', 2)->nullable(true);
            $table->char('country', 2)->nullable(true);
            $table->char('stateprov', 80)->nullable(true);
            $table->char('district', 80)->nullable(true);
            $table->char('city', 80)->nullable(true);
            $table->char('zipcode', 20)->nullable(true);
            $table->float('latitude')->nullable(true);
            $table->float('longitude')->nullable(true);
            $table->integer('geoname_id')->unsigned()->nullable(true);
            $table->float('timezone_offset')->nullable(true);
            $table->char('timezone_name', 64)->nullable(true);
            $table->char('weather_code', 10)->nullable(true);
            $table->char('isp_name', 128)->nullable(true);
            $table->integer('as_number')->unsigned()->nullable(true);
            $table->enum('connection_type', ['dialup', 'isdn', 'cable', 'dsl', 'fttx', 'wireless'])->nullable(true);
            $table->char('organization_name', 128)->nullable(true);
            $table->timestamps();
            $table->charset = 'utf8mb4';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dbip_lookup');
    }
}
